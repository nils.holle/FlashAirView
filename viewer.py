import os
import sys
from subprocess import run, Popen
from time import sleep

dname = "/media/flashair/DCIM/100__TSB"

random = len(sys.argv) > 1 and sys.argv[1] == "random"

if random:
    p = Popen(["feh", dname, "-D 5", "-R 10", "-Z", "-F", "-z"])
else:
    p = None
    last_file = None

while True:
    fList = []

    for f in os.listdir(dname):
        absPath = os.path.join(dname, f)

        if f.lower().endswith(".cr2"):
            size = os.path.getsize(absPath)
            if size < 200000:
                print("Downloading", f)
                run(["flashair-command", "--download", absPath])
            fList.append(f)

    fList = sorted(fList)

    if not random and fList[-1] != last_file:

        if p is not None:
            p.kill()
        last_file = fList[-1]
        p = Popen(["feh", os.path.join(dname, fList[-1]), "-F"])

    sleep(5)
